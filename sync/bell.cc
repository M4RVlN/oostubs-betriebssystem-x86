#include "bell.h"

#include "thread/scheduler.h"
#include "bellringer.h"

void Bell::ring() {
	while (!this->waitinglist.is_empty()) {
		scheduler.wakeup(this->waitinglist.dequeue());
	}
}

void Bell::sleep(unsigned int ms) {
	Bell bell;
	bellringer.job(&bell, ms);

	scheduler.block(&bell);
}