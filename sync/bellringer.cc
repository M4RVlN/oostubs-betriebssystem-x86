#include "sync/bellringer.h"

#include "thread/scheduler.h"

Bellringer bellringer;

void Bellringer::check() {
	Bell *bell = this->bells.first();
	while (bell) {
		if (bell->counter <= 1) {
			bell->ring();
			this->bells.dequeue();
			bell = this->bells.next(*bell);
		} else {
			--bell->counter;
			bell = nullptr;
		}
	}
}

void Bellringer::job(Bell *bell, unsigned int ms) {
	if (this->bells.is_empty()) {
		bell->counter = ms;
		this->bells.enqueue(*bell);
	} else {
		Bell *next = this->bells.first();
		Bell *last = nullptr;
		unsigned counter = 0;

		// find place to insert
		while (next) {
			if (counter + next->counter >= ms) break;

			counter += next->counter;

			last = next;
			next = this->bells.next(*next);
		}

		bell->counter = ms - counter;

		// fix delta for all following bells
		if (bell->counter != 0) {
			while (next) {
				next->counter -= bell->counter;
				next = this->bells.next(*next);
			}
		}

		if (last) {
			this->bells.insertAfter(*last, *bell);
		} else {
			this->bells.insertFirst(*bell);
		}
	}
}

void Bellringer::cancel(Bell *bell) { this->bells.remove(bell); }

bool Bellringer::bellPending() { return !this->bells.is_empty(); }