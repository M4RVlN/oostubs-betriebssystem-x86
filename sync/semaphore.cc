#include "sync/semaphore.h"

void Semaphore::p() {
	if (counter > 0) {
		--this->counter;
	} else {
		scheduler.block(this);
	}
}

void Semaphore::v() {
	if (this->waitinglist.is_empty()) {
		++this->counter;
	} else {
		scheduler.wakeup(this->waitinglist.dequeue());
	}
}