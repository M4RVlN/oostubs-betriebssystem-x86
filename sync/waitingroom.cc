#include "sync/waitingroom.h"
#include "thread/scheduler.h"

Waitingroom::Waitingroom() : waitinglist() {}

void Waitingroom::remove(Thread* customer) {
	waitinglist.remove(customer);
	customer->setWaitingroom(nullptr);
}

Waitingroom::~Waitingroom() {
	while (Thread* thread = waitinglist.dequeue()) {
		scheduler.wakeup(thread);
	}
}