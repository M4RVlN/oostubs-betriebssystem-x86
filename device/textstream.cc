#include "textstream.h"

void TextStream::flush() {
	TextWindow::print(buffer, pos);
	pos = 0;
}