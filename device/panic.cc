// vim: set noet ts=4 sw=4:
#include "panic.h"

#include "debug/kernelpanic.h"

bool Panic::prologue() {
	kernelpanic("Unhandled Interrupt");
	return false;
}