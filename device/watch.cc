#include "device/watch.h"

#include "debug/output.h"
#include "interrupt/plugbox.h"
#include "machine/lapic.h"
#include "thread/scheduler.h"
#include "sync/bellringer.h"

bool Watch::prologue() { return true; }

void Watch::epilogue() {
	bellringer.check();
	scheduler.resume();
}

bool Watch::windup(uint32_t us) {
	delay = us;
	uint32_t ticks_per_ms = LAPIC::Timer::ticks();	// Ticks per millisecond

	dout << "Tick(ms): " << ticks_per_ms << endl;

	uint64_t ticks = static_cast<uint64_t>(us) * ticks_per_ms / 1000;

	uint64_t divide = 1 + ticks / UINT32_MAX;

	// Round up to the next highest power of 2
	divide--;
	divide |= divide >> 1;
	divide |= divide >> 2;
	divide |= divide >> 4;
	divide |= divide >> 8;
	divide |= divide >> 16;
	divide |= divide >> 32;
	divide++;

	// Bigger than 8 bit divider
	if (divide > UINT8_MAX) return false;

	uint32_t counter = ticks / divide;

	dout << "Counter: " << counter << endl;
	dout << "Divider: " << divide << endl;

	Plugbox::assign(Core::Interrupt::Vector::TIMER, this);
	LAPIC::Timer::set(counter, divide, Core::Interrupt::Vector::TIMER, true);
	return true;
}

uint32_t Watch::interval() const { return delay; }

void Watch::activate() const {}