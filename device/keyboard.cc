#include "keyboard.h"

#include "debug/output.h"
#include "interrupt/plugbox.h"
#include "machine/apic.h"
#include "machine/ioapic.h"
#include "machine/ps2controller.h"
#include "machine/system.h"

void Keyboard::plugin() {
	// Register interrupt handler at plugbox
	Plugbox::assign(Core::Interrupt::Vector::KEYBOARD, this);

	// Register device at IOAPIC
	auto slot = APIC::getIOAPICSlot(APIC::Device::KEYBOARD);
	IOAPIC::config(slot, Core::Interrupt::Vector::KEYBOARD, IOAPIC::TriggerMode::LEVEL);

	// Drain buffer before enabling interrupts
	PS2Controller::drainBuffer();

	IOAPIC::allow(slot);
}

bool Keyboard::prologue() { return PS2Controller::fetch(current_key); }

void Keyboard::epilogue() {
	if (current_key.ctrl() && current_key.alt() && current_key.scancode == Key::Scancode::KEY_DEL) {
		System::reboot();
	} else if (buffer.produce(current_key)) {
		key_available.v();
	}
}

bool Keyboard::hasKey() { return !buffer.is_empty(); }

Key Keyboard::getKey() {
	key_available.p();
	Key key;
	buffer.consume(key);
	return key;
}