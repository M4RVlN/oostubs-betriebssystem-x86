/*! \file
 *  \brief \ref Serial \ref SerialStream "output stream"
 */

#pragma once

#include "object/outputstream.h"
#include "machine/serial.h"

/*! \brief Console (VT100 compatible) via \ref Serial interface.
 *  \ingroup io
 *
 * This class allows to connect a VT100-compatible display terminal via
 * the serial interface.
 *
 * The utility 'screen' can be used to attach a terminal to an interface
 * at a specified connection speed: `screen /dev/ttyS0 115200`
 *
 * Color and position can be adjusted with the help of
 * [escape codes](http://web.archive.org/web/20181008150037/http://www.termsys.demon.co.uk/vtansi.htm).
 */

class SerialStream : public OutputStream, public Serial {
	/*! \brief Helper to send a multi-digit number as human readable ASCII characters
	 * \param num Number to send
	 */
	void write_number(int num);

 public:
	/*! \brief Attributes
	 * can be used to influence the display of the output.
	 *
	 * \note The attributes might not be supported or have a different effect
	 *       depending on the terminal emulator!
	 */
	enum Attrib {
		RESET      = 0,  ///< Turn off character attributes
		BRIGHT     = 1,  ///< Bold
		DIM        = 2,  ///< Low intensity (dimmed)
		UNDERSCORE = 4,  ///< Underline
		BLINK      = 5,  ///< Blink (slow)
		REVERSE    = 7,  ///< Swap fore & background
		HIDDEN     = 8,  ///< Concealed
	};

	/*! \brief Color codes
	 *
	 * Default VT100 supports eight colors for both foreground and background
	 * (later versions 256 [8 bit] and even true color [32 bit]).
	 * The actual color is affected by the attributes and can look significantly
	 * different depending on the terminal emulator.
	 */
	enum Color {
		BLACK   = 0,
		RED     = 1,
		GREEN   = 2,
		YELLOW  = 3,
		BLUE    = 4,
		MAGENTA = 5,
		CYAN    = 6,
		WHITE   = 7
	};

	/*! \brief Constructor for the VT100-compatible console
	 *
	 * Sets up the serial connection as well
	 *
	 * \todo Implement Method
	 */
	explicit SerialStream(ComPort port = COM1, BaudRate baud_rate = BAUD_115200, DataBits data_bits = DATA_8BIT,
	                      StopBits stop_bits = STOP_1BIT, Parity parity = PARITY_NONE);

	/*! \brief Method to output the buffer contents of the base class \ref Stringbuffer
	 *
	 * The method is automatically called when the buffer is full,
	 * but can also be called explicitly to force output of the current buffer.
	 *
	 *\todo Implement Method
	 */
	void flush();

	/*! \brief Change foreground color (for subsequent output)
	 *
	 * \todo Implement Method
	 *
	 *  \param c Color
	 */
	void setForeground(Color c);

	/*! \brief Change background color (for subsequent output)
	 *
	 * \todo Implement Method
	 *
	 *  \param c Color
	 */
	void setBackground(Color c);

	/*! \brief Change text attribute (for subsequent output)
	 *
	 * \todo Implement Method
	 *
	 *  \param a Attribute
	 */
	void setAttribute(Attrib a);

	/*! \brief Reset terminal
	 *
	 * Clear screen, place cursor at the beginning and reset colors
	 * and attributes to the default value.
	 *
	 * \todo Implement Method
	 */
	void reset();

	/*! \brief Set the cursor position
	 *
	 *  \param x Column in window
	 *  \param y Row in window
	 *
	 * \todo Implement Method
	 */
	void setPos(int x, int y);

	/*! \brief Display multiple characters in the window starting at the current cursor position
	 *
	 * This method can be used to output a string, starting at the current cursor
	 * position. Since the string does not need to contain a '\0' termination
	 * (as it is usually the case in C), the parameter `length` is required to
	 * specify the number of characters in the string.
	 *
	 * The text is displayed using the previously configured
	 * \ref setAttribute() "attributes", \ref setForeground() "fore-"
	 * and \ref setBackground "background" color.
	 *
	 * A line break will occur wherever the character `\n` is inserted
	 * in the text to be output (for compatibility reasons a `\r` is
	 * automatically appended).
	 *
	 * \param str String to output
	 * \param length length of string
	 */
	void print(char* str, int length);

};
