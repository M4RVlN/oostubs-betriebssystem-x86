// vim: set noet ts=4 sw=4:

/*! \file
 *  \brief Debug macros enabling debug output on a separate window.
 */

#pragma once

/*! \def DBG_VERBOSE
 * \brief An output stream, which is only displayed in the debug window in verbose mode
 *
 * \note If a serial console has been implemented, the output can be redirected
 *       to the serial stream instead (by changing the macro) -- this makes the
 *       (usually) very large output more readable (since it allows scrolling back)
 */
#ifdef VERBOSE
// If VERBOSE is defined, forward everything to \ref DBG
#define DBG_VERBOSE DBG
#else
// Otherwise sent everything to the NullStream (which will simply discard everything)
#define DBG_VERBOSE nullstream
// in this case we have to include the null stream
#include "debug/nullstream.h"
#endif

/*! \def DBG
 *  \brief An output stream, which is displayed in the debug window
 *
 * In single core (\OOStuBS) this is just an alias to the debug window object
 * `dout`.
 */
#define DBG *copyout

#include "device/textstream.h"

/*! \brief Debug window
 *
 * Debug output using \ref DBG like
 *      `DBG << "var = " << var << endl`
 * should be displayed in separate window.
 *
 * Ideally, this window should be placed below the normal output window
 * without any overlap and be able to display 4 lines.
 *
 *  \todo Define `dout`
 */
extern TextStream dout;

/*! \brief Debug window with copy function to serial
 *
 * Provide an additional layer to ouput one debug output also to serial.
 * While this is a simple CopyStream pointer in the single core case, it is
 * an array in the multi core case, which consists of thre TextStreams and
 * one CopyStream.
 * For that, construction is done like:
 *
 * \code{.cpp}
 *  OutputStream* copyout[Core::MAX]{&dout[0], &dout[1], ...}
 * \endcode
 */
extern OutputStream* copyout;
