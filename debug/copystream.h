/*! \file
 *  \brief \ref CopyStream duplicates \ref OutputStream "output streams"
 */

#pragma once

/*! \brief Duplicate all data passed by the stream operator to two \ref OutputStream "output streams"
 *  \ingroup io
 *
 * Can be used as replacement for any \ref OutputStream -- for example,
 * forwarding the \ref DBG output simultaneously to screen (\ref TextStream) and
 * serial console (\ref SerialStream).
 *
 */
class CopyStream : public OutputStream {
	/*! \brief First recipient
	 */
	OutputStream * first;

	/*! \brief Second recipient
	 */
	OutputStream * second;

 public:
	/*! \brief Constructor
	 *
	 * \param first First recipient for output passed to this object
	 * \param second Second recipient for output passed to this object
	 */
	CopyStream(OutputStream * first, OutputStream * second) : first(first), second(second) {}

	/*! \brief Redirect the buffer to both streams and flush them, too.
	 */
	void flush() override {
		*first << buffer << ::flush;
		*second << buffer << ::flush;
	}
};
