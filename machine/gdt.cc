#include "machine/gdt.h"
#include "machine/core.h"
#include "debug/assert.h"
#include "debug/output.h"

namespace GDT {

// The static 32-bit Global Descriptor Table (GDT)
alignas(16) static SegmentDescriptor protected_mode[] = {
	// NULL descriptor
	{},

	// Global code segment von 0-4GB
	{	/* base  = */ 0x0,
		/* limit = */ 0xFFFFFFFF,
		/* code  = */ true,
		/* ring  = */ 0,
		/* size  = */ SIZE_32BIT },

	// Global data segment von 0-4GB
	{	/* base  = */ 0x0,
		/* limit = */ 0xFFFFFFFF,
		/* code  = */ false,
		/* ring  = */ 0,
		/* size  = */ SIZE_32BIT },

};
extern "C" constexpr Pointer gdt_protected_mode_pointer(protected_mode);

}  // namespace GDT
