[SECTION .text]
[GLOBAL context_switch]
[GLOBAL context_launch]

; context_switch saves the registers in the current context structure
; and reads the registers back from the the next context.
align 8
context_switch:
    mov edx, [esp + 4]
	mov [edx + 0], ebx
	mov [edx + 4], ebp
	mov [edx + 8], edi
	mov [edx + 12], esi
	mov [edx + 16], esp

	mov edx, [esp + 8]
	mov ebx, [edx + 0]
	mov ebp, [edx + 4]
	mov edi, [edx + 8]
	mov esi, [edx + 12]
	mov esp, [edx + 16]
    ret

; context_launch restores the register set from the next context structure.
; It does not save the current registers.
align 8
context_launch:
    mov edx, [esp + 4]
	mov ebx, [edx + 0]
	mov ebp, [edx + 4]
	mov edi, [edx + 8]
	mov esi, [edx + 12]
	mov esp, [edx + 16]
    ret
