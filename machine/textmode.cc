#include "textmode.h"

TextMode::Cell* const TextMode::TEXT_BUFFER_BASE = reinterpret_cast<TextMode::Cell*>(0xb8000);

IOPort index_port(0x3d4);
IOPort data_port(0x3d5);

void TextMode::setCursor(unsigned abs_x, unsigned abs_y) {
	unsigned pos = abs_x + abs_y * TextMode::COLUMNS;

	// Cursor data high
	index_port.outb(14);
	data_port.outb((pos >> 8) & 0xff);
	// Cursor data low
	index_port.outb(15);
	data_port.outb(pos & 0xff);
}

void TextMode::getCursor(unsigned& abs_x, unsigned& abs_y) {
	// Cursor data high
	index_port.outb(14);
	uint16_t ch = data_port.inb();
	// Cursor data low
	index_port.outb(15);
	uint8_t cl = data_port.inb();

	unsigned pos = (ch << 8) | cl;

	abs_x = pos % TextMode::COLUMNS;
	abs_y = (pos - abs_x) / TextMode::COLUMNS;
}

void TextMode::show(unsigned abs_x, unsigned abs_y, char character, Attribute attrib) {
	unsigned i = abs_x + abs_y * TextMode::COLUMNS;
	TEXT_BUFFER_BASE[i] = Cell(character, attrib);
}
