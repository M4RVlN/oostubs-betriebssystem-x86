#include "machine/serial.h"

#include "machine/ioport.h"

Serial::Serial(ComPort port, BaudRate baud_rate, DataBits data_bits, StopBits stop_bits, Parity parity) : port(port) {
	// initialize FIFO mode, no irqs for sending, irq if first byte was received

	// line control, select r/w of divisor latch register
	writeReg(LINE_CONTROL_REGISTER, DIVISOR_LATCH_ACCESS_BIT);
	writeReg(DIVISOR_LOW_REGISTER, baud_rate & 0xff);
	writeReg(DIVISOR_HIGH_REGISTER, (baud_rate >> 8) & 0xff);

	char lcr = 0;
	lcr |= data_bits;
	lcr |= stop_bits;
	lcr |= parity;
	writeReg(LINE_CONTROL_REGISTER, lcr);

	// FIFO: Enable & clear buffers
	writeReg(FIFO_CONTROL_REGISTER, ENABLE_FIFO | CLEAR_RECEIVE_FIFO | CLEAR_TRANSMIT_FIFO);

	// Modem Control: OUT2 (0000 1000) must be set for interrupt
	writeReg(MODEM_CONTROL_REGISTER, OUT_2);
}

void Serial::writeReg(RegisterIndex reg, char out) {
	char i = port + reg;
	IOPort port(i);
	port.outb(out);
}

char Serial::readReg(RegisterIndex reg) {
	char i = port + reg;
	IOPort port(i);
	return port.inb();
}

int Serial::write(char out, bool blocking) {
	writeReg(TRANSMIT_BUFFER_REGISTER, out);
	do {
		char status = readReg(LINE_STATUS_REGISTER);
		// Error during transmission
		if ((status & (OVERRUN_ERROR | PARITY_ERROR | FRAMING_ERROR)) != 0) return -1;
		// Transmitter register empty
		if ((status & TRANSMITTER_EMPTY) != 0) break;
	} while (blocking);
	return 0;
}
