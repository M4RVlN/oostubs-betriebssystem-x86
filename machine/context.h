/*! \file
 *  \brief Functionality required for \ref context_switch "context switching"
 */

/*! \defgroup context Context Switch
 *  \brief Low-Level functionality required for context switching
 */

#pragma once

#include "types.h"

/*! \brief Structure for saving the CPU context when switching coroutines.
 * \ingroup context
 */
struct Context {
	intptr_t ebx;  ///< EBX of the thread
	intptr_t ebp;  ///< EBP of the thread
	intptr_t edi;  ///< EDI of the thread
	intptr_t esi;  ///< ESI of the thread
	void* esp;	   ///< Current stack pointer of the thread
} __attribute__((packed));

/*! \brief Prepares a context for its first activation.
 *
 *  \ingroup context
 *
 *  To allow the execution to start in \ref Thread::kickoff during the first activation,
 *  the stack must be initialized such that it contains all the information required.
 *  This is, most importantly, the function to be called (typically the respective
 *  implementation of \ref Thread::kickoff) and any parameters required by this function.
 *
 *  Additionally to the stack, the initial context is setup in a way that \ref context_switch
 *  and \ref context_launch can work with it.
 *
 *  `prepareContext()` can be implemented in the high-level programming language C++
 *  (in file `context.cc`).
 *
 *  \param tos     Pointer to the top of stack (= address of first byte beyond the memory reserved for the stack)
 *  \param context Reference to the Context structure to be filled
 *  \param kickoff Pointer to the \ref Thread::kickoff function
 *  \param param1  first parameter for \ref Thread::kickoff function
 *
 *  \todo Implement Function (and helper functions, if required)
 */
void prepareContext(void* tos, Context& context, void (*kickoff)(void*), void* param1 = nullptr);

/*! \brief Executes the context switch.
 *
 *  \ingroup context
 *
 *  For a clean context switch, the current register values must be stored in the given context struct.
 *  Subsequently, these values must be restored accordingly from the `next` context struct.
 *
 *  This function must be implemented in assembler in the file `context.asm` (why?).
 *  It must be declared as `extern "C"`, so assembler functions are not C++ name mangled.
 *
 *  \param current Pointer to the structure that the current context will be stored in
 *  \param next    Pointer to the structure that the next context will be read from
 *
 *  \todo Implement Method
 */
extern "C" void context_switch(Context* current, Context* next);

/*! \brief Launch context switching.
 *
 *  To start context switching, the current context (from the boot-routines) is thrown away and
 *  the prepared register values within the given `next` context replace it.
 *
 *  This function must be implemented in assembler in the file `context.asm` (why?).
 *  It must be declared as `extern "C"`, so assembler functions are not C++ name mangled.
 *
 *  \ingroup context
 *
 *  \param next    Pointer to the structure that the next context will be read from
 *
 *  \todo Implement Method
 */
extern "C" void context_launch(Context* next);
