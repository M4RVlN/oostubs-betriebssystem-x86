#include "machine/context.h"

void prepareContext(void* tos, Context& context, void (*kickoff)(void*), void* param1) {
	context.ebx = 0;
	context.esi = 0;
	context.edi = 0;

	void** sp = static_cast<void**>(tos);
	--sp;
	*sp = param1;

	--sp;
	*sp = 0;

	--sp;
	*sp = reinterpret_cast<void*>(kickoff);
	context.ebp = reinterpret_cast<intptr_t>(sp);
	context.esp = sp;
}
