#include "textwindow.h"

void TextWindow::setPos(unsigned rel_x, unsigned rel_y) {
	unsigned abs_x = rel_x + this->from_col;
	unsigned abs_y = rel_y + this->from_row;

	if (use_cursor) {
		TextMode::setCursor(abs_x, abs_y);
	} else {
		cursor_x = abs_x;
		cursor_y = abs_y;
	}
}

void TextWindow::getPos(unsigned& rel_x, unsigned& rel_y) const {
	if (use_cursor) {
		TextMode::getCursor(rel_x, rel_y);
		rel_x -= this->from_col;
		rel_y -= this->from_row;
	} else {
		rel_x = cursor_x - this->from_col;
		rel_y = cursor_y - this->from_row;
	}
}

void TextWindow::scrollUp(char character, Attribute attrib) {
	for (unsigned y = from_row; y <= to_row; ++y) {
		for (unsigned x = from_col; x <= to_col; ++x) {
			unsigned i0 = x + y * COLUMNS;
			unsigned i1 = x + (y + 1) * COLUMNS;

			if (y == to_row) {
				TEXT_BUFFER_BASE[i0] = Cell(character, attrib);
			} else {
				TEXT_BUFFER_BASE[i0] = TEXT_BUFFER_BASE[i1];
			}
		}
	}
}

void TextWindow::lineBreak(unsigned& abs_x, unsigned& abs_y) {
	abs_x = from_col;
	if (++abs_y >= to_row + 1) {
		--abs_y;
		scrollUp();
	}
}

void TextWindow::print(const char* string, size_t length, Attribute attrib) {
	unsigned abs_x;
	unsigned abs_y;
	getPos(abs_x, abs_y);
	abs_x += from_col;
	abs_y += from_row;

	for (size_t i = 0; i < length; ++i) {
		char c = string[i];

		if (c == '\n') {
			lineBreak(abs_x, abs_y);
			continue;
		}

		show(abs_x, abs_y, c, attrib);

		if (++abs_x >= to_col + 1) {
			lineBreak(abs_x, abs_y);
		}
	}

	setPos(abs_x - from_col, abs_y - from_row);
}

void TextWindow::reset(char character, Attribute attrib) {
	for (unsigned y = from_row; y <= to_row; ++y) {
		for (unsigned x = from_col; x <= to_col; ++x) {
			show(x, y, character, attrib);
		}
	}
	setPos(0, 0);
}