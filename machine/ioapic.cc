#include "ioapic.h"

#include "debug/assert.h"
#include "machine/apic.h"
#include "machine/core.h"

namespace IOAPIC {
/*! \brief IOAPIC registers memory mapped into the CPU's address space.
 *
 *  Access to the actual IOAPIC registers can be obtained by performing the following steps:
 *  1. Write the number of the IOAPIC register to the address stored in `IOREGSEL_REG`
 *  2. Read the value from / write the value to the address referred to by `IOWIN_REG`.
 *
 *  \see [IO-APIC manual](intel_ioapic.pdf#page=8)
 */
volatile Index *IOREGSEL_REG = reinterpret_cast<volatile Index *>(0xfec00000);
/// \copydoc IOREGSEL_REG
volatile Register *IOWIN_REG = reinterpret_cast<volatile Register *>(0xfec00010);

// IOAPIC manual, p. 8
const Index IOAPICID_IDX = 0x00;
const Index IOREDTBL_IDX = 0x10;

const uint8_t slot_max = 24;

void init() {
	// Set IOAPICID
	*IOREGSEL_REG = IOAPICID_IDX;
	auto ioapic_id = Identification(*IOWIN_REG);
	ioapic_id.id = APIC::getIOAPICID();
	*IOWIN_REG = ioapic_id.value;

	// Initialize all IOAPIC slots to panic interrupt handler
	for (uint8_t i = 0; i < slot_max; i++) {
		config(i, Core::Interrupt::Vector::PANIC);
	}
}

RedirectionTableEntry read_redtbl(uint8_t slot) {
	Index i = IOREDTBL_IDX + (slot * 2);

	*IOREGSEL_REG = i;
	Register reg_low = *IOWIN_REG;

	*IOREGSEL_REG = i + 1;
	Register reg_high = *IOWIN_REG;

	return RedirectionTableEntry(reg_low, reg_high);
}

void write_redtbl(uint8_t slot, RedirectionTableEntry entry) {
	Index i = IOREDTBL_IDX + (slot * 2);

	*IOREGSEL_REG = i;
	*IOWIN_REG = entry.value_low;

	*IOREGSEL_REG = i + 1;
	*IOWIN_REG = entry.value_high;
}

void config(uint8_t slot, Core::Interrupt::Vector vector, TriggerMode trigger_mode, Polarity polarity) {
	auto entry = read_redtbl(slot);
	entry.vector = vector;
	entry.trigger_mode = trigger_mode;
	entry.polarity = polarity;

	write_redtbl(slot, entry);
}

void allow(uint8_t slot) {
	auto entry = read_redtbl(slot);
	entry.interrupt_mask = InterruptMask::UNMASKED;
	write_redtbl(slot, entry);
}

void forbid(uint8_t slot) {
	auto entry = read_redtbl(slot);
	entry.interrupt_mask = InterruptMask::MASKED;
	write_redtbl(slot, entry);
}

bool status(uint8_t slot) { return read_redtbl(slot).interrupt_mask == InterruptMask::UNMASKED; }
}  // namespace IOAPIC
