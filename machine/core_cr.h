/*! \file
 *  \brief Access to \ref Core::CR "Control Register" of a \ref Core "CPU core"
 */

#pragma once

namespace Core {
/*! \brief Control Register 0
 *
 * \see [ISDMv3, 2.5 Control Registers](intel_manual_vol3.pdf#page=74)
 */
enum CR0 {
	CR0_PE = 1 << 0,   ///< Protected Mode enabled
	CR0_MP = 1 << 1,   ///< Monitor co-processor
	CR0_EM = 1 << 2,   ///< Emulation (no x87 floating-point unit present)
	CR0_TS = 1 << 3,   ///< Task switched
	CR0_ET = 1 << 4,   ///< Extension type
	CR0_NE = 1 << 15,  ///< Numeric error
	CR0_WP = 1 << 16,  ///< Write protect
	CR0_AM = 1 << 18,  ///< Alignment mask
	CR0_NW = 1 << 29,  ///< Not-write through caching
	CR0_CD = 1 << 30,  ///< Cache disable
	CR0_PG = 1 << 31,  ///< Paging
};

/*! \brief Control Register 4
 *
 * \see [ISDMv3, 2.5 Control Registers](intel_manual_vol3.pdf#page=77)
 */
enum CR4 {
	CR4_VME        = 1 << 0,   ///< Virtual 8086 Mode Extensions
	CR4_PVI        = 1 << 1,   ///< Protected-mode Virtual Interrupts
	CR4_TSD        = 1 << 2,   ///< Time Stamp Disable
	CR4_DE         = 1 << 3,   ///< Debugging Extensions
	CR4_PSE        = 1 << 4,   ///< Page Size Extension
	CR4_PAE        = 1 << 5,   ///< Physical Address Extension
	CR4_MCE        = 1 << 6,   ///< Machine Check Exception
	CR4_PGE        = 1 << 7,   ///< Page Global Enabled
	CR4_PCE        = 1 << 8,   ///< Performance-Monitoring Counter enable
	CR4_OSFXSR     = 1 << 9,   ///< Operating system support for FXSAVE and FXRSTOR instructions
	CR4_OSXMMEXCPT = 1 << 10,  ///< Operating System Support for Unmasked SIMD Floating-Point Exceptions
	CR4_UMIP       = 1 << 11,  ///< User-Mode Instruction Prevention
	CR4_VMXE       = 1 << 13,  ///< Virtual Machine Extensions Enable
	CR4_SMXE       = 1 << 14,  ///< Safer Mode Extensions Enable
	CR4_FSGSBASE   = 1 << 16,  ///< Enables the instructions RDFSBASE, RDGSBASE, WRFSBASE, and WRGSBASE.
	CR4_PCIDE      = 1 << 17,  ///< PCID Enable
	CR4_OSXSAVE    = 1 << 18,  ///< XSAVE and Processor Extended States Enable
	CR4_SMEP       = 1 << 20,  ///< Supervisor Mode Execution Protection Enable
	CR4_SMAP       = 1 << 21,  ///< Supervisor Mode Access Prevention Enable
	CR4_PKE        = 1 << 22,  ///< Protection Key Enable
};

/*! \brief Access to the Control Register
 *
 * \see [ISDMv3, 2.5 Control Registers](intel_manual_vol3.pdf#page=73)
 * \tparam id Control Register to access
 */
template<uint8_t id>
class CR {
 public:
	/*! \brief Read the value of the current Control Register
	 *
	 * \return Value stored in the CR
	 */
	inline static uintptr_t read(void) {
		uintptr_t val;
		asm volatile("mov %%cr%c1, %0" : "=r"(val) : "n"(id));
		return val;
	}

	/*! \brief Write a value into the current Control Register
	 *
	 * \param value Value to write into the CR
	 */
	inline static void write(uintptr_t value) {
		asm volatile("mov %0, %%cr%c1" : : "r"(value), "n"(id));
	}
};
}  // namespace Core
