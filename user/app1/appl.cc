
#include "user/app1/appl.h"

#include "global.h"
#include "thread/scheduler.h"
#include "syscall/guarded_bell.h"

static unsigned count;
void Application::action() {
	while (true) {
		if (count++ % 1000 == 0) {
			out << "App " << this->get_id() << ", Sleep " << this->get_id() << "s, Tick " << count << endl;
			GuardedBell::sleep(this->get_id() * 1000);
		}
	}
}
