// vim: set noet ts=4 sw=4:

/*! \file
 *  \brief Enthält die Klasse Application
 */

#pragma once

#include "thread/thread.h"

/*! \brief Test application
 *
 *
 */
class Application : public Thread {
	unsigned id;

	// Prevent copies and assignments
	Application(const Application&) = delete;
	Application& operator=(const Application&) = delete;

   public:
	// Allow move
	Application(Application&&) = default;

	/*! \brief Constructor
	 */
	Application(void* tos, unsigned id) : Thread(tos), id(id) {}

	/*! \brief Contains the application code.
	 *
	 */
	void action() override;

	unsigned get_id() { return this->id; }
};
