// vim: set noet ts=4 sw=4:

/*! \file
 *  \brief Enthält die Klasse KeyboardApplication
 */

#pragma once

#include "thread/thread.h"
#include "syscall/guarded_keyboard.h"

/*! \brief Keyboard Application
 */
class KeyboardApplication : public Thread {
	// Prevent copies and assignments
	KeyboardApplication(const KeyboardApplication&) = delete;
	KeyboardApplication& operator=(const KeyboardApplication&) = delete;

   public:
	/*! \brief Constructor
	 */
	KeyboardApplication(void* tos) : Thread(tos) {}

	/*! \brief Contains the application code.
	 *
	 */
	void action() override;
};
extern GuardedKeyboard keyboard;
