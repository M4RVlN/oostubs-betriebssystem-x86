
#include "user/app2/kappl.h"

GuardedKeyboard keyboard;
TextStream kout(0, TextMode::COLUMNS - 1, 24, 24);

void KeyboardApplication::action() {
	while (true) {
		kout << keyboard.getKey() << flush;
	}
}
