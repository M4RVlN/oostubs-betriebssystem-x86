// vim: set noet ts=4 sw=4:

#include "scheduler.h"

#include "debug/output.h"
#include "sync/waitingroom.h"

Scheduler scheduler;

void Scheduler::schedule() {
	if (readylist.is_empty()) {
		this->dispatcher.go(this->idleThread);
	} else {
		this->dispatcher.go(this->readylist.dequeue());
	}
}

void Scheduler::ready(Thread *that) {
	if (!this->readylist.enqueue(*that)) {
		dout << "Thread is already scheduled" << endl;
	}
}

void Scheduler::exit() {
	if (readylist.is_empty()) {
		this->dispatcher.dispatch(this->idleThread);
	} else {
		this->dispatcher.dispatch(this->readylist.dequeue());
	}
}

void Scheduler::kill(Thread *that) {
	if (!this->readylist.remove(that)) {
		dout << "Could not find Thread to kill" << endl;
	} else {
		that->kill_flag = true;
	}
}

void Scheduler::resume() {
	this->readylist.enqueue(*(this->active()));
	this->exit();
}

bool Scheduler::isEmpty() { return this->readylist.is_empty(); }

void Scheduler::block(Waitingroom *waitingroom) {
	waitingroom->waitinglist.enqueue(*(this->active()));
	this->active()->setWaitingroom(waitingroom);
	this->exit();
}

void Scheduler::wakeup(Thread *customer) {
	auto waitingroom = customer->getWaitingroom();

	if (waitingroom == nullptr) {
		dout << "Tried to wake up a Thread, which is not waiting" << endl;
		return;
	}

	waitingroom->remove(customer);
	this->ready(customer);
}

void Scheduler::setIdle(IdleThread *that) { idleThread = that; }