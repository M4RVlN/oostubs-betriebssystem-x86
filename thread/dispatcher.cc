// vim: set noet ts=4 sw=4:

#include "thread/dispatcher.h"

#include "debug/output.h"

Dispatcher::Dispatcher() : life(nullptr) {}

Thread* Dispatcher::active() { return life; }

void Dispatcher::go(Thread* first) {
	if (!life) {
		life = first;
		life->go();
	} else {
		dout << "Dispater is already running" << endl;
	}
}

void Dispatcher::dispatch(Thread* next) {
	auto old = life;
	life = next;
	old->resume(next);
}