// vim: set noet ts=4 sw=4:

/*! \file
 *  \brief \ref Thread abstraction required for multithreading
 */

/*! \defgroup thread Multithreading
 *  \brief The Multithreading Subsystem
 *
 * The group Multithreading contains all elements that form the foundation
 * of CPU multiplexing. This module's objective is to provide the abstraction
 * thread that provides a virtualised CPU for the user's applications.
 */

#pragma once

#include "machine/context.h"
#include "object/queue.h"

class Waitingroom;

/*! \brief The Thread is an object used by the scheduler.
 *  \ingroup thread
 */
class Thread {
	Thread* queue_link;
	Waitingroom* waitingroom;

   protected:
	Context context;

	/*! \brief Function to start a thread.
	 *
	 *  For the first activation of a thread, we need a "return address"
	 *  pointing to a function that will take care of calling C++ virtual
	 *  methods. For this purpose, we use this `kickoff()` function.
	 *
	 *  <b>Activating kickoff</b>
	 *
	 *  The context initialization via \ref prepareContext() not only initializes
	 *  the Stack for the first thread change, but also pushes the address of
	 *  `kickoff()` as return address to the stack.
	 *  Consequently, the first execution of \ref context_switch() will start
	 *  execution by returning to the beginning of `kickoff()` .
	 *
	 *  This `kickoff()` function simply calls the \ref action() method on the
	 *  thread passed as parameter and, thus, resolves the virtual C++ method.
	 *
	 *  \note As this function is never actually called, but only executed by
	 *        returning from the co-routine's initial stack, it may never return.
	 *        Otherwise garbage values from the stack will be interpreted as
	 *        return address and the system might crash.
	 *
	 *  \param object Thread to be started
	 *
	 *  \todo Implement Method
	 */
	static void kickoff(Thread* object);

   public:
	/*! \brief Marker for a dying thread
	 */
	volatile bool kill_flag;

	/*! \brief Constructor
	 *  Initializes the context using \ref prepareContext with the given stack space.
	 *
	 *  \param tos the top of stack, highest address of some memory block that should be used as stack
	 *  	   (remember stacks grow to the lower addresses on x86).
	 *
	 *  \todo Implement constructor
	 */
	explicit Thread(void* tos);

	/*! \brief Activates the first thread on this CPU.
	 *
	 *  Calling the method starts the first thread on the calling CPU.
	 *  From then on, \ref Thread::resume() must be used all subsequent context
	 *  switches.
	 *
	 *  \todo Implement Method
	 *
	 */
	void go();

	/*! \brief Switches from the currently running thread to the `next` one.
	 *
	 *  The values currently present in the callee-saved registers will be
	 *  stored in this threads context-structure, the corresponding values
	 *  belonging to `next` thread will be loaded.
	 *  \param next Pointer to the next thread.
	 *
	 *  \todo Implement Method
	 */
	void resume(Thread* next);

	/*! \brief Method that contains the thread's program code.
	 *
	 *  Derived classes are meant to override this method to provide
	 *  meaningful code to be run in this thread.
	 */
	virtual void action() = 0;

	Waitingroom* getWaitingroom() const;

	void setWaitingroom(Waitingroom* w);

	friend Queue<Thread>;
};
