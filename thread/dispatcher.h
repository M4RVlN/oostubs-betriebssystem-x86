// vim: set noet ts=4 sw=4:

/*! \file
 *  \brief \ref Dispatcher for \ref Thread threads
 */
#pragma once

#include "machine/core.h"
#include "thread/thread.h"

/*! \brief The dispatcher dispatches threads and puts the scheduler's
 *  decisions into action.
 *  \ingroup thread
 *
 *  The dispatcher manages the life pointer that refers to the currently
 *  active thread and performs the actual switching of processes.
 *  For single-core systems, a single life pointer is sufficient, as only a
 *  single thread can be active at any one time. On multi-core systems,
 *  every CPU core needs its own life pointer.
 */
class Dispatcher {
	Thread* life;

	/*! \brief set the currently active thread
	 *  \param thread active Thread
	 */
	void setActive(Thread* thread) { life = thread; }

   public:
	/*! \brief constructor
	 *
	 *	\todo Implement Method
	 */
	Dispatcher();
	/*! \brief Returns the thread currently running on the CPU core calling
	 *  this method
	 *
	 *
	 *  \todo Implement Method
	 */
	Thread* active();

	/*! \brief This method stores first as life pointer for this CPU core and
	 *  triggers the execution of first. Only to be used for the first thread
	 *  running on a CPU.
	 *  \param first First thread to be executed on this CPU core.
	 *
	 *  \todo Implement Method
	 */
	void go(Thread* first);

	/*! \brief Updates the life pointer to next and issues a thread change from
	 *  the old to the new life pointer.
	 *  \param next Next thread to be executed.
	 *
	 *  \todo Implement Method
	 */
	void dispatch(Thread* next);
};
