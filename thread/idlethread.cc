#include "thread/idlethread.h"

#include "thread/scheduler.h"

void IdleThread::action() {
	Core::Interrupt::disable();
	while (true) {
		if (scheduler.isEmpty()) {
			Core::idle();
		} else {
			Core::Interrupt::enable();
		}
	}
}