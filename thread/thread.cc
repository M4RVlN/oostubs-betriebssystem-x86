// vim: set noet ts=4 sw=4:

#include "thread/thread.h"

#include "debug/output.h"
#include "interrupt/guard.h"
#include "thread/dispatcher.h"

Thread::Thread(void* tos) : queue_link(nullptr), waitingroom(nullptr), context() {
	prepareContext(tos, context, (void (*)(void*))(&kickoff), this);
}

void Thread::kickoff(Thread* object) {
	Guard::leave();
	object->action();
}

void Thread::go() { context_launch(&(this->context)); }

void Thread::resume(Thread* next) { context_switch(&(this->context), &(next->context)); }

Waitingroom* Thread::getWaitingroom() const { return waitingroom; }

void Thread::setWaitingroom(Waitingroom* w) { waitingroom = w; }
