#include "guard.h"

#include "machine/core_interrupt.h"
#include "object/queue.h"

namespace Guard {

bool flag = false;
Queue<Gate> epilogue_queue;

void enter() { flag = true; }

void leave() {
	Core::Interrupt::disable();
	while (!epilogue_queue.is_empty()) {
		auto gate = epilogue_queue.dequeue();
		Core::Interrupt::enable();
		gate->epilogue();
		Core::Interrupt::disable();
	}
	flag = false;
	Core::Interrupt::enable();
}

void relay(Gate& item) {
	Core::Interrupt::disable();

	// If E1/2 is active return, else activate and process E1/2
	if (flag) {
		epilogue_queue.enqueue(item);
		Core::Interrupt::enable();
	} else {
		enter();
		Core::Interrupt::enable();
		item.epilogue();
		leave();
	}
}
}  // namespace Guard