#include "plugbox.h"

namespace Plugbox {

Gate* gate_map[256];

void init(Gate* gate) {
	for (int i = 0; i < 256; ++i) {
		gate_map[i] = gate;
	}
}

void assign(Core::Interrupt::Vector vector, Gate* gate) { gate_map[vector] = gate; }

Gate* report(Core::Interrupt::Vector vector) { return gate_map[vector]; }

}  // namespace Plugbox