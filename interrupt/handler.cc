#include "interrupt/handler.h"

#include "debug/output.h"
#include "device/keyboard.h"
#include "device/panic.h"
#include "interrupt/guard.h"
#include "machine/lapic.h"
#include "plugbox.h"

extern "C" void interrupt_handler(Core::Interrupt::Vector vector, InterruptContext* context) {
	(void)context;
	auto gate = Plugbox::report(vector);
	auto has_epilogue = gate->prologue();
	LAPIC::endOfInterrupt();

	if (has_epilogue) Guard::relay(*gate);
	Core::Interrupt::enable();
}
