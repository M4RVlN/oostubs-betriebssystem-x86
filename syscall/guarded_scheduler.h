// vim: set noet ts=4 sw=4:

/*! \file
 *  \brief \ref GuardedScheduler, a \ref Guarded "guarded" interface for \ref Scheduler
 */

#pragma once

#include "interrupt/guarded.h"
#include "thread/scheduler.h"
#include "thread/thread.h"

/*! \brief \ref Guarded interface to the \ref Scheduler used by user applications.
 *
 * Implements the system call interface for class \ref Scheduler. All methods
 * provided by this class are wrappers for the respective method from the base
 * class, which provide additional synchronization by using the class \ref Guarded.
 */
class GuardedScheduler {
   public:
	GuardedScheduler();

	void exit() {
		Guarded guarded;
		scheduler.exit();
	}

	void ready(Thread *that) {
		Guarded guarded;
		scheduler.ready(that);
	}

	void kill(Thread *that) {
		Guarded guarded;
		scheduler.kill(that);
	}

	void resume() {
		Guarded guarded;
		scheduler.resume();
	}
};
