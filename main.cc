#include "debug/output.h"
#include "device/keyboard.h"
#include "device/panic.h"
#include "device/watch.h"
#include "global.h"
#include "interrupt/guard.h"
#include "interrupt/plugbox.h"
#include "machine/apic.h"
#include "machine/core_interrupt.h"
#include "machine/ioapic.h"
#include "machine/ps2controller.h"
#include "machine/serial.h"
#include "thread/scheduler.h"
#include "user/app1/appl.h"
#include "user/app2/kappl.h"

const char* os_name =
	"OO"
	"StuBS";

Panic panic_gate;
Watch watch;

static void* stack0[256];
static void* stack1[256];
static void* stack2[256];
static void* stack3[256];

// Main function
extern "C" int main() {
	dout << "Debug:" << endl;

	// Serial serial;
	PS2Controller::init();

	// Interrupts
	IOAPIC::init();
	Plugbox::init(&panic_gate);

	keyboard.plugin();

	if (!watch.windup(1000)) {
		dout << "Watch interval timer to long for APIC" << endl;
		return 0;
	}

	// Threads
	IdleThread idle(&stack0[256]);

	Application app1(&stack1[256], 1);
	KeyboardApplication app2(&stack2[256]);
	Application app3(&stack3[256], 5);

	scheduler.setIdle(&idle);

	scheduler.ready(&app1);
	scheduler.ready(&app2);
	scheduler.ready(&app3);

	Core::Interrupt::enable();
	Guard::enter();
	scheduler.schedule();
	return 0;
}
