#include "outputstream.h"

#include "types.h"

#define MK_MAKEUNSIGNED(T, V) \
	template <>               \
	struct make_unsigned<T> { \
	   public:                \
		typedef V type;       \
	};

template <typename T>
struct make_unsigned {
	typedef T type;
};

MK_MAKEUNSIGNED(signed short, unsigned short);
MK_MAKEUNSIGNED(signed int, unsigned int);
MK_MAKEUNSIGNED(signed long, unsigned long);
MK_MAKEUNSIGNED(signed long long, unsigned long long);

#undef MK_MAKEUNSIGNED

template <typename T>
OutputStream& printAsString(OutputStream& os, T ival, int base) {
	char buffer[sizeof(T) + 20];
	int i = 0;

	bool is_neg = ival < 0;
	// Convert to binary without sign
	typename make_unsigned<T>::type number = is_neg ? -ival : ival;

	// Convert to ASCII char
	if (number == 0) {
		buffer[i++] = '0';
	} else {
		while (number > 0) {
			int digit = number % base;

			if (base > 10) {
				buffer[i++] = digit < 10 ? digit + '0' : digit - 10 + 'a';
			} else {
				buffer[i++] = digit + '0';
			}

			number = number / base;
		}
	}

	switch (base) {
		case 2:
			buffer[i++] = 'b';
			buffer[i] = '0';
			break;
		case 8:
			buffer[i] = '0';
			break;
		case 10:
			if (is_neg)
				buffer[i] = '-';
			else
				--i;
			break;
		case 16:
			buffer[i++] = 'x';
			buffer[i] = '0';
			break;
		default:
			break;
	}

	// Write back to os buffer
	for (int c = i; c >= 0; c--) {
		os << buffer[c];
	}

	return os;
}

OutputStream& OutputStream::operator<<(char c) {
	put(c);
	return *this;
}

OutputStream& OutputStream::operator<<(unsigned char c) {
	put(static_cast<char>(c));
	return *this;
}

OutputStream& OutputStream::operator<<(const char* string) {
	const char* ptr = string;
	for (char c = *ptr; c; c = *++ptr) {
		put(c);
	}
	return *this;
}

OutputStream& OutputStream::operator<<(bool b) { return operator<<(b ? "true" : "false"); }

OutputStream& OutputStream::operator<<(short ival) { return printAsString(*this, ival, base); }

OutputStream& OutputStream::operator<<(unsigned short ival) { return printAsString(*this, ival, base); }

OutputStream& OutputStream::operator<<(int ival) { return printAsString(*this, ival, base); }

OutputStream& OutputStream::operator<<(unsigned int ival) { return printAsString(*this, ival, base); }

OutputStream& OutputStream::operator<<(long ival) { return printAsString(*this, ival, base); }

OutputStream& OutputStream::operator<<(unsigned long ival) { return printAsString(*this, ival, base); }

OutputStream& OutputStream::operator<<(long long ival) { return printAsString(*this, ival, base); }

OutputStream& OutputStream::operator<<(unsigned long long ival) { return printAsString(*this, ival, base); }

OutputStream& OutputStream::operator<<(const void* ptr) {
	return printAsString(*this, reinterpret_cast<size_t>(ptr), 16);
}

OutputStream& OutputStream::operator<<(OutputStream& (*f)(OutputStream&)) { return f(*this); }

OutputStream& flush(OutputStream& os) {
	os.flush();
	return os;
}

OutputStream& endl(OutputStream& os) { return os << '\n' << flush; }

OutputStream& bin(OutputStream& os) {
	os.base = 2;
	return os;
}

OutputStream& oct(OutputStream& os) {
	os.base = 8;
	return os;
}

OutputStream& dec(OutputStream& os) {
	os.base = 10;
	return os;
}

OutputStream& hex(OutputStream& os) {
	os.base = 16;
	return os;
}