// vim: set noet ts=4 sw=4:

/*! \file
 *  \brief Contains a \ref BBuffer "bounded buffer"
 */

#pragma once

#include "types.h"
#include "object/bbuffer.h"

/*! \brief The class RingBuffer implements a bounded buffer, that is a circular
 *  buffer with a fixed capacity.
 *
 *  \tparam T the type of data to be stored
 *  \tparam CAP the buffers capacity
 */
template <typename T, unsigned CAP>
class RingBuffer : public BBuffer<T, CAP> {
	// Prevent copies and assignments
	RingBuffer(const RingBuffer&) = delete;
	RingBuffer& operator=(const RingBuffer&) = delete;

   public:
	/*! \brief Constructor that initialized an empty buffer.
	 */
	RingBuffer() : BBuffer<T, CAP>() {}

	/*! \brief Add an element to the buffer.
	 *  \param val The element to be added.
	 *  \return `false` if the buffer is full and an object was overwritten; `true` otherwise.
	 */
	bool produce(T val) {
		if (!BBuffer<T, CAP>::produce(val)) {
			{
				T _void;
				BBuffer<T, CAP>::consume(_void);
			}
			BBuffer<T, CAP>::produce(val);
			return false;
		}
		return true;
	}

	/*! \brief Remove an element from the buffer.
	 * \param val Output parameter that receives the next element. If there is
	 *            (currently) no next element, `val` will not be modified.
	 * \return `false` if the buffer was empty; `true` if the buffer was
	 *          not empty and an element was written to val.
	 */
	bool consume(T& val) { return BBuffer<T, CAP>::consume(val); }

	unsigned size() { return CAP; }
};
